/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author levansen
 */
public class Rectangle {
    
    private double height;
    private double width;
    
public void setHeight(double height){
    this.height = height;
}

public void setWidth(double width){
    this.width = width;
}

    public double getHeigth() {
        return height;
    }

    public double getWidth() {
        return width;
    }
    
    public double getArea(){
        return height * width;
    }
    
    public double getPerimeter(){
        double totalHeight = height*2;
        double totalWidth = width*2;
        return totalHeight+totalWidth;
    }
    
}
